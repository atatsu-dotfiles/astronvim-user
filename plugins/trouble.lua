return {
  "folke/trouble.nvim",
  event = "BufRead",
  config = function()
    require("trouble").setup {}
    local which_key = require("which-key")
    which_key.register({
      ["<leader>x"] = {
        name = "+Trouble",
        d = { "<cmd>TroubleToggle document_diagnostics<cr>", "Document diagnostics" },
        D = { "<cmd>TroubleToggle lsp_definitions<cr>", "LSP definitions" },
        l = { "<cmd>TroubleToggle loclist<cr>", "Loclist" },
        q = { "<cmd>TroubleToggle quickfix<cr>", "Quickfix" },
        R = { "<cmd>TroubleToggle lsp_references<cr>", "LSP references" },
        t = { "<cmd>TroubleToggle<cr>", "Toggle" },
        T = { "<cmd>TroubleToggle lsp_type_definitions<cr>", "LSP type definitions" },
        w = { "<cmd>TroubleToggle workspace_diagnostics<cr>", "Workspace diagnostics" },
      }
    })
    end
}
