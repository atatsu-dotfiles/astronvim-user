return function()
  -- Set autocommands
  -- This autocommand will ensure folds work when opening a file via telescope
  -- but it circumvents the proceeding autocommand that automatically opens all
  -- folds when a new file is read :(
  -- vim.api.nvim_create_autocmd("BufEnter", {
  --   desc = "Update folds",
  --   pattern = "*",
  --   command = "normal zx"
  --})
  --vim.api.nvim_create_autocmd("BufReadPost,FileReadPost", {
  --  desc = "Open all folds when a file is opened",
  --  pattern = "*",
  --  command = "normal zR"
  --})

  -- Set up custom filetypes
  -- vim.filetype.add {
  --   extension = {
  --     foo = "fooscript",
  --   },
  --   filename = {
  --     ["Foofile"] = "fooscript",
  --   },
  --   pattern = {
  --     ["~/%.config/foo/.*"] = "fooscript",
  --   },
  -- }
end
