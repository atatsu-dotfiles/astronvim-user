local astro_utils = require "astronvim.utils"

-- setting a mapping to false will disable it
return {
  -- first key is the mode
  n = {
    ["<c-q>"] = false, -- unmapped
    ["<c-\\>"] = { "<Esc><Cmd>ToggleTerm<CR>", desc = "Toggle term" },
    ["<cr>"] = { "za", desc = "Toggle fold under cursor" },
    -- buffer switching
    ["<F2>"] = {
      function()
        if #vim.t.bufs > 1 then
          require("telescope.builtin").buffers { sort_mru = true, ignore_current_buffer = true }
        else
          astro_utils.notify "No other buffers open"
        end
      end,
      desc = "Switch buffers",
    },
  },
  t = {
    ["<c-\\>"] = { "<Esc><Cmd>ToggleTerm<CR>", desc = "Toggle term" },
    ["<c-q>"] = { "<c-\\><c-n>", desc = "Terminal normal mode" },
  },
}
